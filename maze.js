var MH=6;
var MW=6;
var mazeRatio=0.8;
//Adjust to screen size:
//unit is the size of the square + the size of the rectangle
var unit=bestunit();
//CellSize is the size of the square
var CellSize=Math.floor(unit*mazeRatio);
var BorderWidth=unit-CellSize;

// var renderDistance=500;//Math.floor(Math.sqrt((MH+MW)/2)*5);
var renderDistance=Math.floor(Math.sqrt((MH+MW)/2)*5);
var fadeDelay=200;

var noFade=true;

//Player position:
var ply=Math.floor(Math.random()*MH);
var plx=Math.floor(Math.random()*MW);

//Starting base color:
var baseColor="#00FF00";

//Current color pallete:
var colorPallete=[];

pallete(baseColor,renderDistance);

//Global walls to avoid difficulties:
var walls;

//MOVEMENT
//Constant global keyboard logic:
pressedkeys=new Array(100);
pressedkeys.fill(false);
presseddirections=[false,false,false,false];
//var keyboardLogic=[0,13,1,15,5,0,3,1,9,11,0,13,7,9,5,0]; //Deprecated
var keyboardLogic=[[[[0,13],[1,15]],[[5,0],[3,1]]],[[[9,11],[0,13]],[[7,9],[5,0]]]];

var allowMouse=false;
var mouseControl=false;
//Wait for the maze to display:
setTimeout(function(){allowMouse=true;},2300);
var mousey;
var mousex;

var lastMousey;
var lastMousex;

var lastMove=0;

//On load:
$().ready(function(){
	//Adjust document title:
	document.title=MH+" x "+MW;

	//Create the maze area on screen:
	printBlackMaze(CellSize,BorderWidth);
	
	//Create an array representing all walls (in "3d" coordinates):
	walls=new Array(MH);
	for(var i=0;i<MH;i++){
		walls[i]=new Array(MW);
		for(var j=0;j<MW;j++)
			walls[i][j]=new Array(4);
	}
	
	//Generate the maze:
	generatel(walls);
	//Place exit point:
	genexit(walls,ply,plx);
	
	displayGameState(ply,plx,renderDistance,baseColor,"#FFFFFF",disC,disW);
	
	//Movement options:
	
	setInterval(movement,120);
	
	
	document.getElementById("t").addEventListener("mouseover",trackmouse,false);
	document.getElementById("t").addEventListener("mouseout",untrackmouse,false);
	//$("body").keydown(keyboard(event));
	$("body").keydown(keyboard);
	$("body").keyup(keyboard);
});

//Movement:
function movement(){
	//If the player has moved, update the screen:
	if(controlOptions()){
		//Check if the maze is solved:
		checkSolved();
		//Update screen:
		displayGameState(ply,plx,renderDistance,baseColor,"#FFFFFF",recC,recW);
	}
}
function checkSolved(){
	if(ply<0){
		fog(ply+1,plx,1,renderDistance*2,renderDistance*2-1,baseColor,recC,recW);
		solvedAnimation(3,1500,"easeInCubic");
	}
	if(ply>MH-1){
		fog(ply-1,plx,3,renderDistance*2,renderDistance*2-1,baseColor,recC,recW);
		solvedAnimation(1,1500,"easeInCubic");
	}
	if(plx<0){
		fog(ply,plx+1,0,renderDistance*2,renderDistance*2-1,baseColor,recC,recW);
		solvedAnimation(2,1500,"easeInCubic");
	}
	if(plx>MW-1){
		fog(ply,plx-1,2,renderDistance*2,renderDistance*2-1,baseColor,recC,recW);
		solvedAnimation(0,1500,"easeInCubic");
	}
}
function controlOptions(){
	//Mouse control gets priority:
	if(mouseControl){
		return movePlayerAttempt(mouseAngle());
	}
	else{
		var success=movePlayerAttempt(keyboardAngle());
		if(!(pressedkeys[39]||pressedkeys[68]))presseddirections[0]=false;
		if(!(pressedkeys[38]||pressedkeys[87]))presseddirections[1]=false;
		if(!(pressedkeys[37]||pressedkeys[65]))presseddirections[2]=false;
		if(!(pressedkeys[40]||pressedkeys[83]))presseddirections[3]=false;
		return success;
	}
	
}

//Mouse control:
function trackmouse(e){
	//Keep track of the mouse coordinates:
	mousey=e.clientY;
	mousex=e.clientX;
	if(mousey!=lastMousey||mousex!=lastMousex){
		lastMousey=mousey;
		lastMousex=mousex;
		//If mouse is allowed, activate it:
		mouseControl=allowMouse;
	}
}
function untrackmouse(){
	mouseControl=false;
}
function mouseAngle(){
	//Calculate the distance between the cursor and the player:
	var disty=-mousey+((MH-ply)*unit-CellSize/2);
	var distx=mousex-((plx+1)*unit-CellSize/2);
	
	var closey=(disty>(-unit/2)&&disty<(unit/2));
	var closex=(distx>(-unit/2)&&distx<(unit/2));
	
	//override the "close" thing when mouse is at outer edge
	//so you can easily go into the exit
	if (mousey<unit/2) closey = false;
	if (mousey>MH*unit-CellSize/2) closey = false;
	if (mousex<unit/2) closex = false;
	if (mousex>MW*unit-CellSize/2) closex = false;
	
	//console.log(mousey+", "+mousex+" unit:"+unit+" cs/2:"+(CellSize/2)+" "+disty+", "+distx);
	
	//CHOOSE DIRECTION CODE:
	
	//If the cursor is basically on the player, no movement:
	if(closey&&closex)return 0;
	
	if(closey){ //Horizontal
		if(distx>0)return 1; //Right
		return 9; //Left
	}
	if(closex){ //Vertical
		if(disty>0)return 5; //Up
		return 13; //Down;
	}
	
	//Coefficient:
	var k=disty/distx;
	if(k<-1.73){ //Downwards, very steep
		if(distx>0)return 14;
		return 6;
	}
	if(k<-0.58){ //Downwards, slightly steep
		if(distx>0)return 15;
		return 7;
	}
	if(k<0){ //Downwards, not steep at all
		if(distx>0)return 16;
		return 8;
	}
	if(k<0.58){ //Upwards, not steep at all
		if(distx>0)return 2;
		return 10;
	}
	if(k<1.73){ //Upwards, slightly steep
		if(distx>0)return 3;
		return 11;
	}
	//else: Upwards, very steep
	if(distx>0)return 4;
	return 12;	
}

//Keyboard control:
function keyboard(event){
	pressedkeys[event.keyCode]=event.type=="keydown";
	if(pressedkeys[39]||pressedkeys[68])presseddirections[0]=true;
	if(pressedkeys[38]||pressedkeys[87])presseddirections[1]=true;
	if(pressedkeys[37]||pressedkeys[65])presseddirections[2]=true;
	if(pressedkeys[40]||pressedkeys[83])presseddirections[3]=true;
	mouseControl=false;
}
function keyboardAngle(){
	//Quick access of the keyboard logic:
	return keyboardLogic[presseddirections[2]*1][presseddirections[1]*1][presseddirections[0]*1][presseddirections[3]*1];
	//return keyboardLogic[presseddirections[37]][pressedkeys[38]*1][pressedkeys[39]*1][pressedkeys[40]*1];
}

function movePlayerAttempt(direction){
	switch(direction){
	//If the player is not actually trying to move:
	case 0: return false;
	//Primary directions (1,5,9,13) (elementary):
	case 1: return basicMove(0); //Right
	case 5: return basicMove(1); //Up
	case 9: return basicMove(2); //Left
	case 13: return basicMove(3); //Down
	//Secondary directions (3,7,11,15) (diagonal):
	case 3: return diagMove(0,1); //Up-right
	case 7: return diagMove(1,2); //Up-left
	case 11: return diagMove(2,3); //Down-left
	case 15: return diagMove(0,3); //Down-right
	//Ternary directions (2,4,6,8,10,12,14,16) (primary+secondary):
	case 2: return secondMove(0,1); //Right up
	case 4: return secondMove(1,0); //Up right
	case 6: return secondMove(1,2); //Up left
	case 8: return secondMove(2,1); //Left up
	case 10: return secondMove(2,3); //Left down
	case 12: return secondMove(3,2); //Down left
	case 14: return secondMove(3,0); //Down right
	case 16: return secondMove(0,3); //Right down
	}
}
//Basic movement in one direction:
function basicMove(d){
	//Do not move if there's a wall here:
	if(!cwall(ply,plx,d))return false;
	ply=my(ply,d);
	plx=mx(plx,d);
	lastMove=d;
	return true;
}
//Smart diagonal movement:
function diagMove(d1,d2){
	//Attempt to move in the first direction,
	//unless that was the direction of the previous move:
	if(d1!=lastMove)
		if(basicMove(d1))
			return true;
	if(basicMove(d2))
		return true;
	return basicMove(d1);
}
//Movenent with a primary and secondary direction:
function secondMove(d1,d2){
	//If possible, use the primary direction:
	if(basicMove(d1))return true;
	//Otherwise, use the secondary direction:
	return basicMove(d2);
}

//Create the table on screen:
function printBlackMaze(CS,BW){
	//Get the table:
	var theTable=$("#t");
	
	//Build all rows except the last one:
	for(var i=0;i<MH;i++){
		//A row of inters and hwalls:
		{
			var newtr=$("<tr>");
			//All inters and hwalls except for the last inters:
			for(var j=0;j<MW;j++){
				//One intersection:
				newtr.append(newtd(BW,BW,"i_"+i+"_"+j));
				//One hwall:
				newtr.append(newtd(BW,CS,"hw_"+i+"_"+j));
			}
			//Last intersection:
			newtr.append(newtd(BW,BW,"i_"+i+"_"+MW));
			theTable.prepend(newtr);
		}
		
		//A row of cells and vwalls:
		{
			var newtr=$("<tr>");
			//All cells and vwalls except the last one:
			for(var j=0;j<MW;j++){
				//One vwall:
				newtr.append(newtd(CS,BW,"vw_"+i+"_"+j));
				//One cell:
				newtr.append(newtd(CS,CS,"c_"+i+"_"+j));
			}
			//Last vwall:
			newtr.append(newtd(CS,BW,"vw_"+i+"_"+MW));
			theTable.prepend(newtr);
		}
	}
	
	//Build the last row of inters and hwalls:
	{
		var newtr=$("<tr>");
		//All inters and hwalls except for the last inters:
		for(var j=0;j<MW;j++){
			//One intersection:
			newtr.append(newtd(BW,BW,"i_"+MH+"_"+j));
			//One hwall:
			newtr.append(newtd(BW,CS,"hw_"+MH+"_"+j));
		}
		//Last intersection of the last row:
		newtr.append(newtd(BW,BW,"i_"+MH+"_"+MW));
		theTable.prepend(newtr);
	}
}
//Create and return a new black td with the given attributes:
function newtd(h,w,id){
	var newtd=$("<td>");
	
	var newdiv=$("<div>");
	newdiv.css("height",h);
	newdiv.css("width",w);
	newdiv.attr("id",id);
	newdiv.css("background-color","black");
	
	newtd.append(newdiv);
	return newtd;
}


//Abstraction layer for recoloring and dissolving animations:
function recC(y,x,color){
	if(noFade)instaCell(y,x,color);
	else fadeCell(y,x,color,fadeDelay);
}
function recW(y,x,d,color){
	if(noFade)instaWall(y,x,d,color);
	else fadeWall(y,x,d,color,fadeDelay);
}
function disC(y,x,color){
	if(color=="#FFFFFF")
		fadeCell(y,x,color,500);
	else
		dissolveCell(y,x,color,500,0,1000);
}
function disW(y,x,d,color){
	dissolveWall(y,x,d,color,500,800,1500);
}


//Convert 3d wall coordinates to element on screen:
function wall3d(y,x,d){
	switch(d){
		//Right:
		case 0: return $("#vw_"+y+"_"+(x+1));
		//Up:
		case 1: return $("#hw_"+(y+1)+"_"+x);
		//Left:
		case 2: return $("#vw_"+y+"_"+x);
		//Down:
		default: return $("#hw_"+y+"_"+x);
	}
}

//Recolor a cell by coordinates, with no animation:
function instaCell(y,x,color){
	$("#c_"+y+"_"+x).css("background-color",color);
}
//Recolor a wall by 3d coordinates, with no animation:
function instaWall(y,x,d,color){
	wall3d(y,x,d).css("background-color",color);
}
//Recolor a cell by coordinates, with a fade animation:
function fadeCell(y,x,color,dur){
	$("#c_"+y+"_"+x).animate({backgroundColor: color},dur);
}
//Recolor a wall by 3d coordinates, with a fade animation:
function fadeWall(y,x,d,color,dur){
	wall3d(y,x,d).animate({backgroundColor: color},dur);
}
//Recolor a cell by coordinates, with a dissolve animation:
function dissolveCell(y,x,color,dur,mindelay,maxdelay){
	$("#c_"+y+"_"+x).delay(Math.floor(Math.random()*(maxdelay-mindelay)+mindelay))
		.animate({backgroundColor: color},dur);
}
//Recolor a wall by 3d coordinates, with a dissolve animation:
function dissolveWall(y,x,d,color,dur,mindelay,maxdelay){
	wall3d(y,x,d).delay(Math.floor(Math.random()*(maxdelay-mindelay)+mindelay))
		.animate({backgroundColor: color},dur);
}

//Generate exit:
function genexit(ply,plx){
	//Vertical or horizontal?
	if(Math.random()>0.5){
		//Top or bottom?
		if(2*ply<MH)
			//Top.
			walls[MH-1][Math.floor(Math.random()*MW)][1]=false;
		else
			//Bottom.
			walls[0][Math.floor(Math.random()*MW)][3]=false;
	}
	else{
		//Right or left?
		if(2*plx<MW)
			//Right.
			walls[Math.floor(Math.random()*MH)][MW-1][0]=false;
		else
			//Left.
			walls[Math.floor(Math.random()*MH)][0][2]=false;
	}
}

//Check if a position is free:
function c(map,y,x){
	if(y<0 || y>=MH || x<0 || x>=MW)
		return false;
	return !map[y][x];
}
//Check if a position is ok to move to:
function cwall(y,x,d){
	if(y<0 || y>=MH || x<0 || x>=MW)
		return false;
	return !walls[y][x][d];
}

//Move the x coordinate in a specified direction:
function mx(x,d){
	if(d==0)return x+1;
	if(d==2)return x-1;
	return x;
}
//Move the y coordinate in a specified direction:
function my(y,d){
	if(d==1)return y+1;
	if(d==3)return y-1;
	return y;
}
//Rotate a direction counterclockwise:
function ccw(d){
	return (d+1)%4;
}
//Rotate a direction clockwise:
function cw(d){
	return (d+3)%4;
}
//Reverse a direction:
function rvs(d){
	return (d+2)%4;
}

function displayGameState(y,x,render,basecolor,playercolor,cellani,wallani){
	//A different representation of the render distance:
	render*=2;
	//Color this cell:
	cellani(y,x,playercolor);
	//Color everything else:
	for(var d=0;d<4;d++){
		//Check if it's possible to move there:
		if(cwall(y,x,d)){
			//A wall:
			wallani(y,x,d,colorPallete[render-1]);
			//The entire part of maze in this direction:
			fog(my(y,d),mx(x,d),d,render,render-2,basecolor,cellani,wallani);
		}
	}
}

//Recursive fog display
function fog(y,x,d,render,currender,basecolor,cellani,wallani){
	if(currender<0)return;
	if(y<0 || y>MH-1 || x<0 || x>MW-1)return;
	//Color this cell:
	cellani(y,x,colorPallete[currender]);
	//Color everything else:
	d=cw(d);
	for(var i=0;i<3;i++){
		//Check if it's possible to move there:
		if(cwall(y,x,d)){
			//A wall:
			wallani(y,x,d,colorPallete[currender>0?currender-1:0]);
			//The entire part of maze in this direction:
			fog(my(y,d),mx(x,d),d,render,currender-2,basecolor,cellani,wallani);
		}
		
		//Rotate through the 3 options:
		d=ccw(d);
	}
}

//Create a color pallete for a maze:
function pallete(color,renderDistance){
	for(var i=0;i<=renderDistance*2;i++)
		colorPallete[i]=dim(renderDistance*2,i,color);
}
//Dim a color by bringing it closer to black:
function dim(max,curr,color){
	//Brightness multiplier:
	var m=curr*curr*curr/max/max/max;
	//Split RGB:
	var r=Math.floor(parseInt(color.substr(1,2),16)*m);
	var g=Math.floor(parseInt(color.substr(3,2),16)*m);
	var b=Math.floor(parseInt(color.substr(5,2),16)*m);
	//Fix the lack of leading zeros:
	return "#"+
	((r<16?"0":"")+(r.toString(16)))+
	((g<16?"0":"")+(g.toString(16)))+
	((b<16?"0":"")+(b.toString(16)));
}

function solvedAnimation(d,dur,easing){
	allowMouse=false;
	$("#t").animate(animationDirection(d),dur,easing,function(){
		$("#t").remove();
		var newtable=$("<table>");
		newtable.attr("id","t");
		$("body").append(newtable);
		document.getElementById("t").addEventListener("mouseover",trackmouse,false);
		document.getElementById("t").addEventListener("mouseout",untrackmouse,false);
		
		levelup();
		
		printBlackMaze(CellSize,BorderWidth);
		
		walls=new Array(MH);
		for(var i=0;i<MH;i++){
			walls[i]=new Array(MW);
			for(var j=0;j<MW;j++)
				walls[i][j]=new Array(4);
		}
		
		//Generate the maze:
		generatel();
		
		//Player position:
		ply=Math.floor(Math.random()*MH);
		plx=Math.floor(Math.random()*MW);
		
		//Place exit point:
		genexit(ply,plx);
		
		displayGameState(ply,plx,renderDistance,baseColor,"#FFFFFF",disC,disW);
		
		//Allow mouse after the maze is displayed:
		setTimeout(function(){allowMouse=true;},2300);
	});
}

function animationDirection(d){
	switch(d){
		case 0: return {left: "-150%",};
		case 1: return {top: "150%",};
		case 2: return {left: "150%",};
		default: return {top: "-150%",};
	}
}

function nextcolor(color){
	var rgb=parseInt(color.substring(1),16);
	//Generate the values:
	var c1=(rgb*13371337)%55+200;
	var c2=(rgb*1337666)%255;
	var c3=(rgb*6661337)%55;
	//Assignment:
	var r,g,b;
	switch((rgb*1337)%6){
		case 0: r=c1; g=c2; b=c3; break;
		case 1: r=c1; g=c3; b=c2; break;
		case 2: r=c2; g=c1; b=c3; break;
		case 3: r=c2; g=c3; b=c1; break;
		case 4: r=c3; g=c1; b=c2; break;
		default:r=c3; g=c2; b=c1;
	}
	//Fix the lack of leading zeros:
	return "#"+
	((r<16?"0":"")+(r.toString(16)))+
	((g<16?"0":"")+(g.toString(16)))+
	((b<16?"0":"")+(b.toString(16)));
}

function levelup(){
	//Next color:
	baseColor=nextcolor(baseColor);
	//Next size:
	if((MH+MW)/2<250){
		MH=Math.floor(MH*1.2);
		MW=Math.floor(MW*1.2);
	}
	else{
		MH++;
		MW++;
	}
	//Adjust to screen size:
	unit=bestunit();
	CellSize=Math.floor(unit*mazeRatio);
	BorderWidth=unit-CellSize;
	//Adjust render distance:
	renderDistance=Math.floor(Math.sqrt((MH+MW)/2)*5);
	
	//Adjust color pallete:
	pallete(baseColor,renderDistance);
	
	//Adjust fade delay to make it easier to move:
	fadeDelay=Math.floor(fadeDelay*0.9);
	//Disable delay if the render distance is very long:
	noFade=true;//noFade=renderDistance>20;
	
	//Adjust document title:
	document.title=MH+" x "+MW;
}

//Calculate the optimal unit size while the maze still fits on screen:
function bestunit(){
	var unit=0;
	while(
		(MH*Math.floor(unit*mazeRatio)+(MH+1)*(unit-Math.floor(unit*mazeRatio)) <= $(window).height()) &&
		(MW*Math.floor(unit*mazeRatio)+(MW+1)*(unit-Math.floor(unit*mazeRatio)) <= $(window).width())
	)unit++;
	return --unit;
}

function buttn(d){
	//Only move if there isn't a wall here:
	if(cwall(ply,plx,d)){
		ply=my(ply,d);
		plx=mx(plx,d);
		//Check if the maze is solved:
		if(ply<0 || ply>MH-1 || plx<0 || plx>MW-1){
			fog(my(ply,rvs(d)),mx(plx,rvs(d)),rvs(d),renderDistance,renderDistance-1,baseColor,recC,recW);
			solvedAnimation(d,1500,"easeInCubic");
		}
		//Update screen:
		displayGameState(ply,plx,renderDistance,baseColor,"#FFFFFF",recC,recW);
	}
}
