
//Generate the maze:
function generate(){
	//Initialize walls (all are present):
	for(var i=0;i<MH;i++)
		for(var j=0;j<MW;j++)
			for(var k=0;k<4;k++)
				walls[i][j][k]=true;
	
	//Create an array representing all cells
	//for keeping track of visited cells:
	var map=new Array(MH);
	for(var i=0;i<MH;i++){
		map[i]=new Array(MW);
		for(var j=0;j<MW;j++)
			map[i][j]=false;
	}
	
	//Random starting position:
	var y=Math.floor(Math.random()*MH);
	var x=Math.floor(Math.random()*MW);
	
	var stacktop=0;
	var stacky=new Array(MH*MW);
	var stackx=new Array(MH*MW);
	
	//Maze generation loop:
	do{
		//Mark this cell as visited:
		map[y][x]=true;
		//Count available directions:
		var count=0;
		for(var d=0;d<4;d++){
			if(c(map,my(y,d),mx(x,d)))
				count++;
		}
		
		//If there are possible ways to go, choose one:
		if(count>0){
			//Choose a random one of the available directions:
			var d=Math.floor(Math.random()*4);
			var rotate=Math.random();
			
			while(!c(map,my(y,d),mx(x,d))){
				if(rotate>0.5)d=ccw(d);
				else d=cw(d);
			}
			
			//Move, destroy walls and push the new cell to the stack:
			walls[y][x][d]=false;
			y=my(y,d);
			x=mx(x,d);
			
			walls[y][x][rvs(d)]=false;
			
			stacktop++;
			stacky[stacktop]=y;
			stackx[stacktop]=x;
		}
		
		//If there are no possible ways to go, backtrack:
		else{
			if(stacktop!=0){
				stacktop--;
				y=stacky[stacktop];
				x=stackx[stacktop];
			}
		}
		//If the stack is empty, we are done with generation.
	}while(stacktop!=0);
}

//Generate the maze with loops:
function generatel(){
	//Initialize walls (all are present):
	for(var i=0;i<MH;i++)
		for(var j=0;j<MW;j++)
			for(var k=0;k<4;k++)
				walls[i][j][k]=true;
	
	//Create an array representing all cells
	//for keeping track of visited cells:
	var map=new Array(MH);
	for(var i=0;i<MH;i++){
		map[i]=new Array(MW);
		for(var j=0;j<MW;j++)
			map[i][j]=false;
	}
	
	//Random starting position:
	var originy=Math.floor(Math.random()*MH);
	var originx=Math.floor(Math.random()*MW);
	var y=originy;
	var x=originx;
	
	//A stack of coordinates for backtracking:
	var stacktop=-1;
	var stacky=new Array(MH*MW);
	var stackx=new Array(MH*MW);
	
	//A "stack" to keep track of dead ends and their lengths:
	var deadcount=-1;
	var deadx=new Array(MH*MW);
	var deady=new Array(MH*MW);
	var deadlength=new Array(MH*MW);
	
	//A stack to keep track of intersections (breakpoints for dead ends)
	var interstacktop=-1;
	var interstacky=new Array(MH*MW);
	var interstackx=new Array(MH*MW);
	
	//A flag for possible dead end:
	var isDeadEnd=false;
	//A flag for hall between a dead end and the nearest intersection:
	var deadEndHall=false;
	//A flag for backtracking (when it goes from false to true is when an intersection is added to teh stack):
	var backtracking=false;
	
	//Maze generation loop:
	do{
		//Mark this cell as visited:
		map[y][x]=true;
		//Count available directions:
		var count=0;
		for(var d=0;d<4;d++){
			if(c(map,my(y,d),mx(x,d)))
				count++;
		}
		
		//If there are possible ways to go, choose one:
		if(count>0){
		
			//From here on dead ends are an option:
			isDeadEnd=true;
			
			//If we just went from backtracking to carving AND
			//this is the last time we will ever visit this intersection (apart from backtracking):
			if(backtracking&&count==1){
				//Memorize this intersection, to allow dead end measurement:
				//PUSH:
				interstacktop++;
				interstacky[interstacktop]=y;
				interstackx[interstacktop]=x;
				
				//And we aren't backtracking any more.
				backtracking=false;
			}
			
			//Choose a random one of the available directions:
			var d=Math.floor(Math.random()*4);
			var rotate=1+(Math.random()>0.5)*2; //1 is ccw, 3 is cw
			
			while(!c(map,my(y,d),mx(x,d)))
				d=(d+rotate)%4;
			
			//Move, destroy walls and push the new cell to the stack:
			walls[y][x][d]=false;
			y=my(y,d);
			x=mx(x,d);
			
			walls[y][x][rvs(d)]=false;
			
			//PUSH:
			stacktop++;
			stacky[stacktop]=y;
			stackx[stacktop]=x;
		}
		
		//If there are no possible ways to go, backtrack:
		else{
			backtracking=true;
			
			//Keep track of dead ends:
			if(isDeadEnd){
				//New dead end
				//PUSH:
				deadcount++;
				deadx[deadcount]=x;
				deady[deadcount]=y;
				deadlength[deadcount]=1; //Its length is 1 at first
				
				//No more dead ends, since we are backtracking right now:
				isDeadEnd=false;
				//But we ARE in a dead end hall. Start it:
				deadEndHall=true;
			}
			else{
				//If we are still in a dead end:
				if(deadEndHall){
					//If we found an intersection in the maze which was earlier stored in the stack:
					if(y==interstacky[interstacktop]&&x==interstackx[interstacktop]){
						deadEndHall=false;
						//Remove the breakpoint:
						//POP:
						interstacktop--;
					}
					else
						//Extend the existing dead end:
						deadlength[deadcount]++;
				}
			}
			
			//Backtrack using the stack:
			//POP:
			if(stacktop!=-1){
				stacktop--;
				y=stacky[stacktop];
				x=stackx[stacktop];
			}
		}
		//If the stack is empty, we are done with generation.
	}while(stacktop!=-1);
	
	//The generation origin always ends up as another dead end.
	//Add it to the dead ends:
	//PUSH:
	deadcount++;
	deadx[deadcount]=originx;
	deady[deadcount]=originy;
	deadlength[deadcount]=0; //Its length is 0 at first
	
	//Measure this dead end:
	//Find the first direction:
	var d=0;
	while(walls[originy][originx][d])d++;
	//Follow to the first intersection:
	while(true){
		//Expand:
		deadlength[deadcount]++;
		originy=my(originy,d);
		originx=mx(originx,d);
		
		//Find the available directions:
		var count=0;
		var sample=0; //Stores one of the found directions. After all, this only matters if exactly one exists!
		if(!walls[originy][originx][d]){ sample=d; count++; }
		if(!walls[originy][originx][(d+1)%4]){ sample=(d+1)%4; count++; }
		if(!walls[originy][originx][(d+3)%4]){ sample=(d+3)%4; count++; }
		
		//If this is an intersection, we are done.
		//(this also takes care of the extreme case where there are no intersections in the maze at all)
		if(count!=1)break;
		
		//If there is only one direction to go, it is stored in sample:
		d=sample;
	}
	
	//Since we'll only work with a fraction of the dead ends, they need to be sorted.
	
	//Print dead ends:
	var stringg="";
	for(var i=0;i<=deadcount;i++){
		stringg+=deadlength[i]+", ";
	}
	console.log(stringg);
	
}

function quicksort(){
	
}





