# Fog Maze

# >>> https://mazie.rocks/fog-maze/ <<<

![maze.png](https://img.ourl.ca/maze.png)

It's a maze generator and a maze solving game, with keyboard or mouse controls, and a few basic animations.

# How to play

https://mazie.rocks/fog-maze/

I hope that link is still working when you see this.

If it's not, you can do this:

- download the files index.html mazegeneration.js jquery.min.js jquery-ui.min.js and maze.js
- open the file index.html with your browser (right click and "open with")
